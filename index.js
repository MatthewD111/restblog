//Configure basic app settings + dependencies
var express = require("express"),
    bodyParser = require("body-parser"),
    mongoose = require("mongoose"),
    methodOverride = require("method-override"),
    app = express();
mongoose.connect("mongodb://localhost/restfulblogapp");
app.set("view engine", "ejs");
app.use(express.static("public"));
app.use(bodyParser.urlencoded({extended: true}));
app.use(methodOverride("_method"));

//Mongoose model configuration
var blogSchema = new mongoose.Schema({
    title:String,
    image:String,
    body:String,
    created: { type: Date, default: Date.now() },
    edited: Date
});

var Blog = mongoose.model("Blog", blogSchema);

/*Code to create new items
Blog.create({
    title: "Test Blog",
    image: "https://images.unsplash.com/photo-1471623817296-aa07ae5c9f47",
    body: "Hello, this is a blog post!"
});
*/

//routes (RESTful!)
app.get("/", function(req, res){
    res.redirect("/blog");
}); // redirect to home page (/blog) from /

app.get("/blog", function(req, res){
   Blog.find({},function(err, blogs){
       if(err){
           console.log(err);
       } else {
           res.render("index", {blogs:blogs});
       }
   });
});

//new blog route
app.get("/blog/new", function(req, res) {
   res.render("new");
});
//post route
app.post("/blog", function(req,res){
    Blog.create(req.body.blog, function(err, newBlog){
      if(err){
          res.render("new");
      }  else {
          res.redirect("blogs");
      }
    });
});

//SHOW ROUTE

app.get("/blog/:id", function(req, res){
    Blog.findById(req.params.id, function(err, foundBlog){
        if(err){
            res.redirect("/blog");
        } else {
            res.render("show", {blog: foundBlog});
        }
    });
});


//EDIT ROUTE
app.get("/blog/:id/edit", function(req, res){
    Blog.findById(req.params.id, function(err, foundBlog){
       if(err){
           res.redirect("/blog");
       } else{
           res.render("edit", {blog:foundBlog});
       }
    });
});
//UPDATE ROUTE
app.put("/blog/:id", function(req, res){
    Blog.findByIdAndUpdate(req.params.id, req.body.blog, function(err, updatedBlog){
        if(err){
            res.redirect("/blog");
        } else {
            res.redirect("/blog/" + req.params.id)
        }
    });

});
//DESTROY ROUTE
app.delete("/blog/:id", function(req, res){
    //destroy blog :(
    Blog.findByIdAndRemove(req.params.id, function(err){
        if(err){
            res.redirect("/blog")
        }
        else{
            res.redirect("/blog")
        }
    })
})
//listen in right place!
app.listen(3000, function(){
    console.log("server running...");
});
